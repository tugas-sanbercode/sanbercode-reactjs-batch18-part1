// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var jawaban_1 = kataPertama + ' ' + kataKedua + ' ' + kataKetiga + ' ' + kataKeempat;
console.log('//soal 1');
console.log('  Jawaban soal 1: ' + jawaban_1); //jawaban soal 1

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var jawaban_2 = Number(kataPertama) + Number(kataKedua) + Number(kataKetiga) + Number(kataKeempat);
console.log('//soal 2');
console.log('  Jawaban soal 2: ' + jawaban_2); //jawaban soal 2

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 


var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

//jawaban soal 3
console.log('//soal 3');
console.log('  Jawaban soal 3: '); 
console.log('  Kata Pertama: ' + kataPertama); 
console.log('  Kata Kedua: ' + kataKedua); 
console.log('  Kata Ketiga: ' + kataKetiga); 
console.log('  Kata Keempat: ' + kataKeempat); 
console.log('  Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 65;
var index = '';

if( nilai >= 80 )
	index = 'A';
else if(nilai >= 70 && nilai < 80 )
	index = 'B';
else if(nilai >= 60 && nilai < 70 )
	index = 'C';
else if( nilai >= 50 && nilai < 60 )
	index = 'D';
else if(nilai < 50 )
	index = 'E';

console.log('//soal 4');
console.log('  Jawaban soal 4: ' + index); //jawaban soal 4

//soal 5
var tanggal = 6;
var bulan = 12;
var tahun = 1988;
var bulan_string = '';

switch(bulan){
	case 1:
			bulan_string = 'Januari';
		break;
	case 2:
			bulan_string = 'Februari';
		break;
	case 3:
			bulan_string = 'Maret';
		break;
	case 4:
			bulan_string = 'April';
		break;
	case 5:
			bulan_string = 'Mei';
		break;
	case 6:
			bulan_string = 'Juni';
		break;
	case 7:
			bulan_string = 'Juli';
		break;
	case 8:
			bulan_string = 'Agustus';
		break;
	case 9:
			bulan_string = 'September';
		break;
	case 10:
			bulan_string = 'Oktober';
		break;
	case 11:
			bulan_string = 'November';
		break;
	case 12:
			bulan_string = 'Desember';
		break;
	
	
}

jawaban_5 = tanggal + ' ' + bulan_string + ' ' + tahun;
console.log('//soal 5');
console.log('  Jawaban soal 5: ' + jawaban_5); //jawaban soal 5

