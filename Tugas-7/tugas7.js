// soal 1

// -> release 0
class Animal {
	constructor(name, legs=4, cold_blooded=false){
		this.name=name;
		this.legs=legs;
		this.cold_blooded=cold_blooded;
	}
}
 
var sheep = new Animal("shaun");
 
console.log("Soal 1");
console.log("release 0");
console.log("======================================");
console.log("");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// -> release 1

class Ape extends Animal{
	
	constructor(name, legs=2, cold_blooded){
		
		super(name, legs, cold_blooded);
	}
	
	yell(){
		console.log('Auoo');
	}
}

class Frog extends Animal{
	
	constructor(name, legs, cold_blooded=true){
		
		super(name, legs, cold_blooded);
	}
	
	jump(){
		console.log('hop hop');
	}
}

console.log("");
console.log("");
console.log("Soal 1");
console.log("release 1");
console.log("======================================");
console.log("");
 
var sungokong = new Ape("kera sakti")
sungokong.yell(); // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// soal 2
class Clock{
  
  // var timer;
  
  constructor(template){
	  this.obj = template;
  }
  
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;
	
    var output = this.obj.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  };

  start() {
    this.render();
    this.timer = setInterval(()=>this.render(), 1000);
  };

}

console.log("");
console.log("");
console.log("Soal 2");
console.log("======================================");
console.log("");
var clock = new Clock({template: 'h:m:s'});
clock.start(); 