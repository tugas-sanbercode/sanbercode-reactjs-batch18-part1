
//Soal 1
var index = 2;
var flag = 0;
var text_to_show = '';

console.log("Soal 1");
console.log("======================================");
console.log("");
while((flag==0) || index > 0 && (index <= 22 && flag>=1 && flag <=2)){
	
	if(flag==0){
		flag=1;
	}	
	
	if(flag==1 && index==2){
		console.log("LOOPING PERTAMA");
	}else if(flag==2 && index==20){
		console.log("LOOPING KEDUA");
	}
	
	if(flag==1){
		text_to_show='I love coding';
	}else if(flag==2){
		text_to_show='I will become a frontend developer';
	}
	
	if(index >=2 && index <=20)
		console.log(index.toString() + ' - ' + text_to_show);
	
	switch(flag){
		
		case 1: index+=2; break;			
		case 2: index-=2; break;
	}
	
	if(index>=22){
		flag++;
	}
}

// Soal 2

var text_to_show = '';

console.log("");
console.log("");
console.log("Soal 2");
console.log("======================================");
console.log("");

for(var i=1; i<=20; i++){
	
	if(i%3==0 && i%2==1){
		text_to_show='I Love Coding';
	}else if(i%2==0){
		text_to_show='Berkualitas';
	}else if(i%2==1){
		text_to_show='Santai';
	}
	
	console.log(i.toString() + ' - ' + text_to_show);
}

// Soal 3

var kress='';

console.log("");
console.log("");
console.log("Soal 3");
console.log("======================================");
console.log("");

for(var i=0; i<7; i++){
	
	kress='#';
	
	for(var j=0; j<i; j++){
		
		kress=kress+'#';
	}
	
	console.log(kress);
}

// Soal 4

console.log("");
console.log("");
console.log("Soal 4");
console.log("======================================");
console.log("");

var kalimat="saya sangat senang belajar javascript"

var array = kalimat.split(' ');
console.log(array);

// Soal 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

console.log("");
console.log("");
console.log("Soal 5");
console.log("======================================");
console.log("");

var sorted = daftarBuah.sort();

console.log(sorted);