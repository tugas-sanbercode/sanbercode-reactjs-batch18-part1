//Soal 1

const r = 10;

const luasLingkaran = (r) =>{
	
	let hasil = 22/7 * Math.pow(r,2)
	return hasil;
}

const kelilingLingkaran = (r) =>{
	
	let hasil = 2 * 22/7 * r
	return hasil;
}

console.log("");
console.log("");
console.log("Soal 1");
console.log("======================================");
console.log(""); 
console.log('luas lingkaran: ' + luasLingkaran(r));
console.log('keliling lingkaran: ' + kelilingLingkaran(r));

//soal 2

let kalimat = "";

const tambahKata = (kata) =>{
	
	if(!kalimat){
		kalimat = kata;
	}else{
		kalimat = `${kalimat} ${kata}`;
	}
}

tambahKata( 'saya' );
tambahKata( 'adalah' );
tambahKata( 'seorang' );
tambahKata( 'frontend' );
tambahKata( 'developer' );

console.log("");
console.log("");
console.log("Soal 2");
console.log("======================================");
console.log(""); 
console.log(kalimat);

// soal 3

// const newFunction = function literal(firstName, lastName){
  // return {
    // firstName: firstName,
    // lastName: lastName,
    // fullName: function(){
      // console.log(firstName + " " + lastName)
      // return 
    // }
  // }
// }

const newFunction = (firstName, lastName) => {
	
	return {
		firstName,
		lastName,
		fullName: function(){
			console.log(firstName + " " + lastName)
		}
	}
}
 
console.log("");
console.log("");
console.log("Soal 3");
console.log("======================================");
console.log(""); 
newFunction("William", "Imoh").fullName() 

//soal 4

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log("");
console.log("");
console.log("Soal 4");
console.log("======================================");
console.log(""); 
console.log(firstName);
console.log(lastName);
console.log(destination);
console.log(occupation);
console.log(spell);

// soal 5

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east];

console.log("");
console.log("");
console.log("Soal 5");
console.log("======================================");
console.log(""); 
console.log(combined)