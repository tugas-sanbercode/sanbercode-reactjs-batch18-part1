//Soal 1

function halo(){
	return "Halo Sanbers!";
}

console.log("Soal 1");
console.log("======================================");
console.log(""); 
console.log(halo());

// Soal 2

function kalikan(num1, num2){
	
	return num1 * num2;
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)

console.log("");
console.log("");
console.log("Soal 2");
console.log("======================================");
console.log(""); 
console.log(hasilKali)

// Soal 3

function introduce(name, age, address, hobby){
	
	return "Nama saya "+ name +", umur saya "+ age.toString() +" tahun, alamat saya di "+ address +", dan saya punya hobby yaitu "+ hobby +"!" ;
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)

console.log("");
console.log("");
console.log("Soal 3");
console.log("======================================");
console.log(""); 	
console.log(perkenalan);

// Soal 4

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var obj = {};

arrayDaftarPeserta.forEach(function(item){
	
	index = arrayDaftarPeserta.indexOf(item);
	
	switch(index){
		case 0: obj.nama=item; break;
		case 1: obj["jenis kelamin"]=item; break;
		case 2: obj.hobi=item; break;
		case 3: obj["tahun lahir"]=item; break;
	}
});

console.log("");
console.log("");
console.log("Soal 4");
console.log("======================================");
console.log("");
console.log(obj);	

// Soal 5

var obj = [{
	nama: "strawberry",
	warna: "merah",
	"ada bijinya": "tidak",
	harga: 9000,
}, {
	nama: "jeruk",
	warna: "oranye",
	"ada bijinya": "ada",
	harga: 8000,
}, {
	nama: "Semangka",
	warna: "Hijau & Merah",
	"ada bijinya": "ada",
	harga: 10000,
}, {
	nama: "Pisang",
	warna: "Kuning",
	"ada bijinya": "tidak",
	harga: 5000,
}];

console.log("");
console.log("");
console.log("Soal 5");
console.log("======================================");
console.log(""); 
console.log(obj[0]);

// Soal 6

var dataFilm = [];

function tambahDataFilm(nama, durasi, genre, tahun){
	
	dataFilm.push({
		nama: nama,
		durasi: durasi,
		genre: genre,
		tahun: tahun,
	});
}

tambahDataFilm( 'Inception', '2 jam', 'action', 2010 );
tambahDataFilm( 'Spiderman', '1,5 jam', 'action', 2002 );

console.log("");
console.log("");
console.log("Soal 6");
console.log("======================================");
console.log(""); 
console.log(dataFilm);